# README #

git 연습장.

### What is this repository for? ###
* git branch 연습해본다
* git merge 를 연습해본다
* pull request 를 연습해본다.

### How do I get set up? ###
* git clone -b <branch> <ths repo> 로 특정 branch를 clone 한다.
* git clone -b <branch> --single-branch 로 특정 branch만 clone한다. 

### How do I get changes form master into branch
* git checkout <branch>
* git rebase master

### TODOs
* master 에서 branch를 만들고 작업 시작
* branch 에서 작업 중 master의 변경을 반영해보기
* branch 작업 중 다른 branch(ex. hotfix) 열고 작업하기
* branch 간 이동하기 (stash는생략, 가급적 commit 하고 움직이기)
* branch 작업 완료 후 push하고 pull request 한다
* 동료 검토 후 pull request 를 호스팅서비스(ex. github, bitbucket)에서 merge함

### Contribution guidelines ###

* git clone 한다 
* 새 branch를 만든다
* 작업 완료 후 해당 branch를 push하고 pull request 한다.
